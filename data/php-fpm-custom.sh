#!/usr/bin/env bash

if [ "$XDEBUG_ENABLE" -gt "0" ]; then
  export HOST_IP="`/sbin/ip route|awk '/default/ { print $3 }'`" && \
   echo "xdebug.remote_host=$HOST_IP" >> $PHP_INI_DIR/conf-available/xdebug.ini

  ln -s $PHP_INI_DIR/conf-available/xdebug.ini $PHP_INI_DIR/conf.d/docker-php-ext-xdebug.ini
fi

php-fpm $@