# PHP-FPM 7.x PHALCON 3.x

## info
see https://hub.docker.com/r/amsdard/php/

`php -m`
```
[PHP Modules]
bcmath, Core, ctype, curl, date, dom, ereg, fileinfo, filter, gd, gettext, hash, iconv, imagick, intl, json, libxml, mbstring, mcrypt, memcache, mongo, mysqlnd, openssl, pcre, PDO, mysql, pdo_mysql, pdo_sqlite, Phar, posix, readline, Reflection, session, SimpleXML, SPL, sqlite3, standard, tokenizer, xml, xmlreader, xmlwriter, zip, zlib
```

## features
* works as Your local user (thanks to "UID mapping")
* full i18n support (locale, intl, gettext)
* able to run composer & vegaser
* secure ImageMagick (own policy.xml)
* xdebug disabled by default

## xdebug
To enable xdebug, add a new environment variable `XDEBUG_ENABLE=1`.

To re-configure xdebug, update the `/usr/local/etc/php/conf-available/xdebug.ini` file.

To run xdebug on Your PC (PHPStorm):
* use [xDebug on PHPStorm using Docker](https://www.webcodegeeks.com/devops/debug-php-docker-phpstorm-xdebug/) tutorial (keep in mind xdebug is already installed and configured inside the image and You should enable it via the `XDEBUG_ENABLE` env)
* [optional] adjust `XDEBUG_CONFIG` environment var if needed, by the correct `remote_host` IP value (or host). You may use [connect from container to localhost IP](https://stackoverflow.com/a/38753971) tutorial. 

  By default `xdebug.remote_host` param's value is the result of `/sbin/ip route|awk '/default/ { print $3 }'` command.
* see an example of docker-compose PHP (for Mac):
```
services:
  php:
    image: amsdard/phalcon
    container_name: php
    volumes:
      - ./:/opt
    environment:
       XDEBUG_ENABLE: 1
       XDEBUG_CONFIG: "remote_host=docker.for.mac.localhost"
```
